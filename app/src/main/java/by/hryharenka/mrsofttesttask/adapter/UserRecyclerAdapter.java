package by.hryharenka.mrsofttesttask.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import by.hryharenka.mrsofttesttask.R;
import by.hryharenka.mrsofttesttask.entity.User;

public class UserRecyclerAdapter extends RecyclerView.Adapter<UserRecyclerAdapter.MyViewHolder> {

    Activity activity;

    List<User> userList;

    public UserRecyclerAdapter(Activity activity, List<User> userList) {
        this.activity = activity;
        this.userList = userList;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.user, parent, false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        User user = userList.get(position);
        holder.id.setText(String.valueOf(user.getId()));
        holder.firstName.setText(user.getFirstName());
        holder.secondName.setText(user.getSecondName());
        holder.age.setText(String.valueOf(user.getAge()));
        holder.email.setText(user.getEmail());
    }


    @Override
    public int getItemCount() {
        return userList.size();
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView id;
        private TextView firstName;
        private TextView secondName;
        private TextView age;
        private TextView email;

        private MyViewHolder(View v) {
            super(v);
            id = v.findViewById(R.id.user_id);
            firstName = v.findViewById(R.id.first_name);
            secondName = v.findViewById(R.id.second_name);
            age = v.findViewById(R.id.age);
            email = v.findViewById(R.id.email);


        }

    }


}
