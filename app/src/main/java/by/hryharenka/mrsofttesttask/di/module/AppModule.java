package by.hryharenka.mrsofttesttask.di.module;

import javax.inject.Singleton;

import by.hryharenka.mrsofttesttask.mvp.MainPresenter;
import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    @Provides
    @Singleton
    public MainPresenter provideDaggerPresenter(){
        return new MainPresenter();
    }
}
