package by.hryharenka.mrsofttesttask.di.module;

import android.app.Application;
import android.content.Context;

import java.util.concurrent.Executors;

import javax.inject.Singleton;

import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;
import by.hryharenka.mrsofttesttask.db.TestDB;
import by.hryharenka.mrsofttesttask.entity.User;
import dagger.Module;
import dagger.Provides;
import io.reactivex.annotations.NonNull;

@Module
public class RoomModule {

    private TestDB testDB;

    Context context;

    public RoomModule(Application  application) {
        this.context = application.getApplicationContext();
    }

    @Provides
    @Singleton
    public TestDB getDB() {
        testDB = Room.databaseBuilder(context, TestDB.class, TestDB.DATABASE_NAME)
                .addCallback(new RoomDatabase.Callback() {
                    public void onCreate(@NonNull SupportSQLiteDatabase db) {
                        Executors.newSingleThreadScheduledExecutor().execute(new Runnable() {
                            @Override
                            public void run() {
                                User andreiUser = new User();
                                andreiUser.setId(1);
                                andreiUser.setFirstName("Andrei");
                                andreiUser.setSecondName("Hryharenka");
                                andreiUser.setAge(30);
                                andreiUser.setEmail("st.vifelso@gmail.com");
                                testDB.userDao().insertUser(andreiUser);

                                User misha = new User();
                                misha.setId(2);
                                misha.setFirstName("Misha");
                                misha.setSecondName("Ivanov");
                                misha.setAge(35);
                                misha.setEmail("mivanov@gmail.com");
                                testDB.userDao().insertUser(misha);

                                User alex = new User();
                                alex.setId(3);
                                alex.setFirstName("Alex");
                                alex.setSecondName("Ryabov");
                                alex.setAge(27);
                                alex.setEmail("aryabov@gmail.com");
                                testDB.userDao().insertUser(alex);

                                User johny = new User();
                                johny.setId(4);
                                johny.setFirstName("Johny");
                                johny.setSecondName("Mnemonic");
                                johny.setAge(39);
                                johny.setEmail("j0hny@gmail.com");
                                testDB.userDao().insertUser(johny);

                                User anita = new User();
                                johny.setId(5);
                                johny.setFirstName("Anita");
                                johny.setSecondName("Roberts");
                                johny.setAge(37);
                                johny.setEmail("ani@gmail.com");
                                testDB.userDao().insertUser(johny);

                            }
                        });
                    }
                })
                .allowMainThreadQueries()
                .build();
        return testDB;
    }


}