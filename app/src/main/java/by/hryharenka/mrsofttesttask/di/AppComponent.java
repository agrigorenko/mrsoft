package by.hryharenka.mrsofttesttask.di;

import javax.inject.Singleton;

import by.hryharenka.mrsofttesttask.activity.MainActivity;
import by.hryharenka.mrsofttesttask.db.TestDB;
import by.hryharenka.mrsofttesttask.di.module.AppModule;
import by.hryharenka.mrsofttesttask.di.module.RoomModule;
import by.hryharenka.mrsofttesttask.mvp.MainPresenter;
import dagger.Component;

@Singleton
@Component(modules = {RoomModule.class, AppModule.class})
public interface AppComponent {

    TestDB getDB();
    MainPresenter daggerPresenter();

    void inject(MainActivity activity);
    void inject(MainPresenter presenter);

}