package by.hryharenka.mrsofttesttask.activity;

import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import org.angmarch.views.NiceSpinner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import by.hryharenka.mrsofttesttask.App;
import by.hryharenka.mrsofttesttask.R;
import by.hryharenka.mrsofttesttask.adapter.UserRecyclerAdapter;
import by.hryharenka.mrsofttesttask.entity.User;
import by.hryharenka.mrsofttesttask.mvp.MainPresenter;
import by.hryharenka.mrsofttesttask.mvp.view.MainView;
import dagger.Lazy;


public class MainActivity extends AppCompatActivity implements MainView {
    private static final String USER_STATE_KEY = "users";
    private List<String> fields = new ArrayList<>();
    private final static int ID_INDEX = 0;
    private final static int FIRST_NAME_INDEX = 1;
    private final static int SECOND_NAME_INDEX = 2;
    private final static int AGE_INDEX = 3;
    private final static int EMAIL_INDEX = 4;


    private TextView textView;

    private EditText emailEditText;

    private RecyclerView mRecyclerView;

    private UserRecyclerAdapter userRecyclerAdapter;

    private List<User> userList = new ArrayList<>();

    @Inject
    Lazy<MainPresenter> daggerPresenter;

    @InjectPresenter
    MainPresenter presenter;

    @ProvidePresenter
    public MainPresenter providePresenter() {
        presenter = daggerPresenter.get();
        return presenter;
    }

    NiceSpinner niceSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        textView = findViewById(R.id.text);
        emailEditText = findViewById(R.id.input_email);
        mRecyclerView = findViewById(R.id.user_list);
        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(), layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);
        userRecyclerAdapter = new UserRecyclerAdapter(this, userList);
        mRecyclerView.setAdapter(userRecyclerAdapter);
        App.getAppComponent().inject(this);
        niceSpinner = findViewById(R.id.nice_spinner);
        fields = Arrays.asList(getResources().getStringArray(R.array.fields));
        niceSpinner.attachDataSource(fields);
        niceSpinner.setOnSpinnerItemSelectedListener((parent, view, position, id) -> {
            String item = String.valueOf(parent.getItemAtPosition(position));
            int index = fields.indexOf(item);
            Collections.sort(userList, new FieldBasedComparator(index));
            userRecyclerAdapter.notifyDataSetChanged();
        });

        emailEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable edit) {
                if (edit.length() != 0) {
                    presenter.getUsersByEmail(edit.toString());
                } else {
                    presenter.getUsers();
                }
            }
        });

        if (savedInstanceState != null){
            replaceOldListWithNewList(Objects.requireNonNull(savedInstanceState.getParcelableArrayList(USER_STATE_KEY)));
        }

    }




    private class FieldBasedComparator implements Comparator<User> {
        private final int index;

        private FieldBasedComparator(int index) {
            this.index = index;
        }

        public int compare(User user1, User user2) {
            int compare;
            switch (index){
                case ID_INDEX:
                    compare = Integer.compare(user1.getId(), user2.getId());
                    break;
                case FIRST_NAME_INDEX:
                    compare = user1.getFirstName().compareToIgnoreCase(user2.getFirstName());
                    break;
                case SECOND_NAME_INDEX:
                    compare = user1.getSecondName().compareToIgnoreCase(user2.getSecondName());
                    break;
                case AGE_INDEX:
                    compare = Integer.compare(user1.getAge(), user2.getAge());
                    break;
                case EMAIL_INDEX:
                    compare = user1.getEmail().compareToIgnoreCase(user2.getEmail());
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + index);
            }
            return compare;

        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        //moxy issue(https://github.com/Arello-Mobile/Moxy/issues/100)
        providePresenter();
        presenter.attachView(this);
        presenter.getUsers();
    }

    @Override
    public void loadUsers(List<User> users) {
        replaceOldListWithNewList(users);
    }



    private void replaceOldListWithNewList(List<User> users) {
        userList.clear();
        userList.addAll(users);
        userRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onSaveInstanceState(Bundle icicle) {
        super.onSaveInstanceState(icicle);
        ArrayList<? extends Parcelable> parcelables = (ArrayList<? extends Parcelable>) userList;
        icicle.putParcelableArrayList(USER_STATE_KEY, parcelables);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        replaceOldListWithNewList(Objects.requireNonNull(savedInstanceState.getParcelableArrayList(USER_STATE_KEY)));
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //moxy issue(https://github.com/Arello-Mobile/Moxy/issues/100)
        presenter.destroyView(this);
    }
}
