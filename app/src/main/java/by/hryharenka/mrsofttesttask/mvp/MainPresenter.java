package by.hryharenka.mrsofttesttask.mvp;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import by.hryharenka.mrsofttesttask.App;
import by.hryharenka.mrsofttesttask.db.TestDB;
import by.hryharenka.mrsofttesttask.entity.User;
import by.hryharenka.mrsofttesttask.mvp.view.MainView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

@InjectViewState
public class MainPresenter extends MvpPresenter<MainView> {
    @Inject
    TestDB db;

    @Override
    public void attachView(MainView view) {
        super.attachView(view);
        App.getAppComponent().inject(this);
    }

    public void getUsers() {
        Disposable subscribe = db.userDao().fetchAllUsers()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::dbUserResponse);
    }

    public void getUsersByEmail(String prefixEmail) {
        Disposable subscribe = db.userDao().fetchAllUsersByEmail(prefixEmail)
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::dbUserResponse);
    }

    private void dbUserResponse(List<User> users) {
        getViewState().loadUsers(users);
    }

}
