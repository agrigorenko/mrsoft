package by.hryharenka.mrsofttesttask;

import android.app.Application;

import javax.inject.Singleton;

import by.hryharenka.mrsofttesttask.di.DaggerAppComponent;
import by.hryharenka.mrsofttesttask.di.AppComponent;
import by.hryharenka.mrsofttesttask.di.module.AppModule;
import by.hryharenka.mrsofttesttask.di.module.RoomModule;

@Singleton
public class App extends Application {

    private static AppComponent sAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        sAppComponent =  DaggerAppComponent.builder()
                .roomModule(new RoomModule(this))
                .appModule(new AppModule())
                .build();

    }

    public static AppComponent getAppComponent() {
        return sAppComponent;
    }

}