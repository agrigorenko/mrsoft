package by.hryharenka.mrsofttesttask.mvp.view;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import by.hryharenka.mrsofttesttask.entity.User;

public interface MainView extends MvpView {

   void loadUsers(List<User> users);

}
