package by.hryharenka.mrsofttesttask.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import by.hryharenka.mrsofttesttask.dao.UserDao;
import by.hryharenka.mrsofttesttask.entity.User;

@Database(entities = {User.class}, version = 1, exportSchema = false)
public abstract class TestDB extends RoomDatabase {

    public static final String DATABASE_NAME = "user_db";

    public abstract UserDao userDao();

}