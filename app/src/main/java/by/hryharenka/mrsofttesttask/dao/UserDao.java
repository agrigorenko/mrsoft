package by.hryharenka.mrsofttesttask.dao;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import by.hryharenka.mrsofttesttask.entity.User;
import io.reactivex.Observable;

@Dao
public interface UserDao {
    String USER_TABLE = "test_user";

    @Insert
    void insertUser(User user);

    @Query("SELECT * FROM " + USER_TABLE)
    Observable<List<User>> fetchAllUsers();

    @Query("SELECT * FROM " + USER_TABLE + " WHERE email LIKE :prefixEmail || '%'")
    Observable<List<User>> fetchAllUsersByEmail(String prefixEmail);

}
